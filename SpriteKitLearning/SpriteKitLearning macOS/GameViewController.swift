//
//  GameViewController.swift
//  SpriteKitLearning macOS
//
//  Created by Nicolas Dennu on 11/09/2022.
//

import Cocoa
import SpriteKit
import GameplayKit

class GameViewController: NSViewController {
	override func viewDidLoad() {
		super.viewDidLoad()

		guard let frame = NSScreen.main?.frame else { return }
//		let scene = GameScene(size: frame.size)
		let scene = GameScene(size: .init(width: 400, height: 400))
		guard let skView = self.view as? SKView else { return }

		skView.showsFPS = true
		skView.showsNodeCount = true
		skView.ignoresSiblingOrder = true
		scene.scaleMode = .aspectFill
		skView.presentScene(scene)
	}
}
