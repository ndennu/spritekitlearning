//
//  Direction.swift
//  SpriteKitLearning
//
//  Created by Nicolas Dennu on 11/09/2022.
//

import Foundation

enum Direction: UInt16 {
	/// Should be Keyboard key code Z or W
	case up = 13

	/// Should be Keyboard key code S
	case down = 1

	/// Should be Keyboard key code Q or A
	case left = 0

	/// Should be Keyboard key code D
	case right = 2
}
