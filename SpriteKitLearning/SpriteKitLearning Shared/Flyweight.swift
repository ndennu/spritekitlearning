//
//  Flyweight.swift
//  SpriteKitLearning
//
//  Created by Nicolas Dennu on 12/09/2022.
//

import SpriteKit

protocol Texturable {
	var texture: [SKTexture] { get }
//	func render(from location: CGPoint, to newLocation: CGPoint)
}

final class TextureFlyweight: Texturable {
	let texture: [SKTexture]

	init(texture: [SKTexture]) {
		self.texture = texture
	}
}

final class TextureFlyweightFactory {
	var availableTexture = [TexturableType : Texturable]()

	func getTexture(type: TexturableType) -> Texturable {
		if let texture = availableTexture[type] {
			return texture
		} else {
			return createTexture(type: type)
		}
	}

	private func createTexture(type: TexturableType) -> Texturable {
		var flyweight: TextureFlyweight
		switch type {
			case .mainCharacter(let charType):
				switch charType {
					case .up(let moveType):
						switch moveType {
							case .static:
								flyweight = TextureFlyweight(texture: [SKTexture(imageNamed: "main-back")])

							case .dynamic(let names):
								let textures = names.map({
									SKTexture(imageNamed: $0)
								})
								flyweight = TextureFlyweight(texture: textures)
						}
				}

//				let flyweight = TextureFlyweight(texture: SKTexture(imageNamed: "main-back"))
				availableTexture[type] = flyweight
				return flyweight
		}
	}
}

enum TexturableType: Hashable {
	case mainCharacter(MainCharacterType)
}

enum MainCharacterType: Hashable {
	case up(MoveType)
//	case down(MoveType)
//	case left(MoveType)
//	case right(MoveType)
}

enum MoveType: Hashable {
	case `static`
	case dynamic([String])
}
