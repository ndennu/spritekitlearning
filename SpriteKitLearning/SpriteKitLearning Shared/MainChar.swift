//
//  MainChar.swift
//  SpriteKitLearning
//
//  Created by Nicolas Dennu on 12/09/2022.
//

import SpriteKit

final class MainChar: SKSpriteNode {
	var direction: Direction? = .down
	var velocity = CGPoint.zero

	var texturable: Texturable?

	convenience init(texturable: Texturable) {
		self.init(texture: texturable.texture.first)
		self.texturable = texturable
	}

	func walk(animatedTextures: [SKTexture]) {
		let action = SKAction.animate(with: animatedTextures, timePerFrame: 0.1)
		run(SKAction.repeatForever(action))
	}

	func stop(texture: SKTexture) {
		velocity = .zero
		removeAllActions()
		self.texture = texture
		direction = nil
	}
}
