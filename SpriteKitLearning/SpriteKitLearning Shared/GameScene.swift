//
//  GameScene.swift
//  SpriteKitLearning Shared
//
//  Created by Nicolas Dennu on 11/09/2022.
//

import SpriteKit

class GameScene: SKScene {

	private var mainChar: MainChar!
//	private var secondChar: MainChar!

	private var lastUpdateTime: TimeInterval = 0
	private var dt: TimeInterval = 0

	private lazy var flyFactory = TextureFlyweightFactory()

	override func didMove(to view: SKView) {
		let texturable = flyFactory.getTexture(type: .mainCharacter(.up(.static)))
		mainChar = MainChar(texturable: texturable)
		mainChar.position = .init(x: size.width / 2, y: size.height / 2)

//		let texturable2 = flyFactory.getTexture(type: .mainCharacter(.up(.static)))
//		secondChar = MainChar(texturable: texturable2)
//		secondChar.position = .init(x: size.width / 4, y: size.height / 2)
		addChild(mainChar)
//		addChild(secondChar)
	}

	override func update(_ currentTime: TimeInterval) {
		if lastUpdateTime > 0 {
			dt = currentTime - lastUpdateTime
		} else {
			dt = 0
		}
		lastUpdateTime = currentTime
		print("\(dt*1000) miliseconds since last update")

		moveSprite(sprite: mainChar)
	}

	private func moveSprite(sprite: SKSpriteNode) {
		let dtFloat = CGFloat(dt)
		let velocity = mainChar.velocity

		let amountToMove = CGPoint(x: velocity.x * dtFloat,
								   y: velocity.y * dtFloat)
		print("Amount to move: \(amountToMove)")

		sprite.position = CGPoint(x: sprite.position.x + amountToMove.x,
								  y: sprite.position.y + amountToMove.y)
	}
}

#if os(OSX)
extension GameScene {
	override func keyDown(with event: NSEvent) {
		guard let direction = Direction(rawValue: event.keyCode),
			  mainChar.direction != direction
		else { return }

		mainChar.direction = direction

		let textures: [SKTexture]
		let velocity: CGPoint

		switch direction {
			case .up:
				velocity = .init(x: 0, y: 64)
				textures = flyFactory.getTexture(type: .mainCharacter(.up(.dynamic(["main-back-1",
																		 "main-back-2"]))))
				.texture
//				textures = [SKTexture(imageNamed: "main-back-1"),
//							SKTexture(imageNamed: "main-back-2")]

			case .down:
				velocity = .init(x: 0, y: -64)
				textures = flyFactory.getTexture(type: .mainCharacter(.up(.dynamic(["main-back-1",
																					"main-back-2"]))))
				.texture
//				textures = [SKTexture(imageNamed: "main-front-1"),
//							SKTexture(imageNamed: "main-front-2")]

			case .left:
				velocity = .init(x: -64, y: 0)
				textures = flyFactory.getTexture(type: .mainCharacter(.up(.dynamic(["main-back-1",
																					"main-back-2"]))))
				.texture
//				textures = [SKTexture(imageNamed: "main-left-1"),
//							SKTexture(imageNamed: "main-left-2")]

			case .right:
				velocity = .init(x: 64, y: 0)
				textures = flyFactory.getTexture(type: .mainCharacter(.up(.dynamic(["main-back-1",
																					"main-back-2"]))))
				.texture
//				textures = [SKTexture(imageNamed: "main-right-1"),
//							SKTexture(imageNamed: "main-right-2")]
		}

		mainChar.velocity = velocity
		mainChar.walk(animatedTextures: textures)
	}

	override func keyUp(with event: NSEvent) {
		guard let direction = mainChar.direction else { return }

		let texture: SKTexture?

		switch direction {
			case .up:
				texture = flyFactory.getTexture(type: .mainCharacter(.up(.static)))
					.texture.first
//				texture = SKTexture(imageNamed: "main-back")

			case .down:
				texture = flyFactory.getTexture(type: .mainCharacter(.up(.static)))
					.texture.first
//				texture = SKTexture(imageNamed: "main-front")

			case .left:
				texture = flyFactory.getTexture(type: .mainCharacter(.up(.static)))
					.texture.first
//				texture = SKTexture(imageNamed: "main-left")

			case .right:
				texture = flyFactory.getTexture(type: .mainCharacter(.up(.static)))
					.texture.first
//				texture = SKTexture(imageNamed: "main-right")
		}

		guard let texture = texture else { return }
		mainChar.stop(texture: texture)
	}
}
#endif
