//
//  GameViewController.swift
//  SpriteKitLearning iOS
//
//  Created by Nicolas Dennu on 11/09/2022.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController { 
	override func viewDidLoad() {
		super.viewDidLoad()

		let scene = GameScene(size: UIScreen.main.bounds.size)
		guard let skView = self.view as? SKView else { return }

		skView.showsFPS = true
		skView.showsNodeCount = true
		skView.ignoresSiblingOrder = true
		scene.scaleMode = .aspectFill
		skView.presentScene(scene)
	}
}
